package com.andreacode.chatt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andreacode.chatt.Current;
import com.andreacode.chatt.R;
import com.andreacode.chatt.adapter.holder.MessageHolder;
import com.andreacode.chatt.data.model.Message;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {

    private List<Message> messageList = new ArrayList<>();
    private Context context;

    public MessageAdapter(Context context) {
        this.context = context;
    }

    public void addMessage(List<Message> messageList) {
        this.messageList = messageList;
        notifyItemInserted(messageList.size());
    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view;

        if (i == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_card_emisor, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_card_receptor, parent, false);
        }

        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
        Message message = messageList.get(position);

        if (message.getUrl_photo() != null) {
            Picasso.get().load(message.getUrl_photo())
                    .resize(1000, 500)
                    .centerInside()
                    .into(holder.getPhoto_message());
        }
        holder.getMessage().setText(message.getContent());
        holder.getTime().setText(message.getTime());

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (messageList.get(position).getSender_id() != null) {

            if (messageList.get(position).getSender_id().equals(Current.user.getUid())) {
                return 1;
            } else {
                return -1;
            }

        } else {
            return -1;
        }
    }
}
