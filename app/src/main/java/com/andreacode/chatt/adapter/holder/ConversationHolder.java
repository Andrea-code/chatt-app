package com.andreacode.chatt.adapter.holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andreacode.chatt.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConversationHolder extends RecyclerView.ViewHolder {

    private CircleImageView civ_sec_user_profile;
    private TextView tv_username;
    private TextView tv_last_message;

    public ConversationHolder(@NonNull View itemView) {
        super(itemView);
        civ_sec_user_profile = itemView.findViewById(R.id.civ_sec_user_profile);
        tv_username = itemView.findViewById(R.id.tv_username);
        tv_last_message = itemView.findViewById(R.id.tv_last_message);
    }

    public CircleImageView getCiv_sec_user_profile() {
        return civ_sec_user_profile;
    }

    public void setCiv_sec_user_profile(CircleImageView civ_sec_user_profile) {
        this.civ_sec_user_profile = civ_sec_user_profile;
    }

    public TextView getTv_username() {
        return tv_username;
    }

    public void setTv_username(TextView tv_username) {
        this.tv_username = tv_username;
    }

    public TextView getTv_last_message() {
        return tv_last_message;
    }

    public void setTv_last_message(TextView tv_last_message) {
        this.tv_last_message = tv_last_message;
    }
}
