package com.andreacode.chatt.adapter.holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.andreacode.chatt.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class UsersHolder extends RecyclerView.ViewHolder {

    private CircleImageView imageUser;
    private TextView titleUser;
    private CardView user_card;


    public UsersHolder(@NonNull View itemView) {
        super(itemView);
        imageUser = itemView.findViewById(R.id.iv_ImageUser);
        titleUser = itemView.findViewById(R.id.tv_titleUser);
        user_card = itemView.findViewById(R.id.cv_user_card);
    }

    public CircleImageView getImageUser() {
        return imageUser;
    }

    public void setImageUser(CircleImageView imageUser) {
        this.imageUser = imageUser;
    }

    public TextView getTitleUser() {
        return titleUser;
    }

    public void setTitleUser(TextView titleUser) {
        this.titleUser = titleUser;
    }

    public CardView getUser_card() {
        return user_card;
    }

    public void setUser_card(CardView user_card) {
        this.user_card = user_card;
    }
}
