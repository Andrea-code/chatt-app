package com.andreacode.chatt.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andreacode.chatt.R;
import com.andreacode.chatt.adapter.holder.ConversationHolder;
import com.andreacode.chatt.data.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationHolder> implements View.OnClickListener {

    private List<User> users;
    private Context context;
    private View.OnClickListener listener;
    private User user;

    public ConversationAdapter(Context context , List<User> users) {
        this.context = context;
        this.users = users;
    }

    @NonNull
    @Override
    public ConversationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_card, parent, false);

        view.setOnClickListener(this);

        return new ConversationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationHolder holder, int position) {
        user = users.get(position);

        holder.getTv_username().setText(user.getName());

        if(user.getProfilePhoto() != null) {
            Picasso.get()
                    .load(user.getProfilePhoto())
                    .into(holder.getCiv_sec_user_profile());
        }

    }

    @Override
    public int getItemCount() {

       /* if(users != null){
            return users.size();
        }
        return 0;*/

        return users.size();
    }

    public List<User> getUsers() {
        return users;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onClick(view);
        }
    }
}
