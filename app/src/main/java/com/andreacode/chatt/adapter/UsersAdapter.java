package com.andreacode.chatt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andreacode.chatt.R;
import com.andreacode.chatt.adapter.holder.UsersHolder;
import com.andreacode.chatt.data.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersHolder> implements View.OnClickListener {

    private View.OnClickListener listener;
    private List<User> users;
    private Context context;
    private User user;

    public UsersAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
    }

    @NonNull
    @Override
    public UsersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_card, parent, false);

        view.setOnClickListener(this);

        return new UsersHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersHolder holder, int position) {
        user = users.get(position);

        holder.getTitleUser().setText(user.getName());

        if (user.getProfilePhoto() != null) {
            Picasso.get()
                    .load(user.getProfilePhoto())
                    .into(holder.getImageUser());
        }
    }

    public List<User> getUsers() {
        return users;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onClick(view);
        }
    }
}
