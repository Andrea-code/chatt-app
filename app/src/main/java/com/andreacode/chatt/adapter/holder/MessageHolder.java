package com.andreacode.chatt.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.andreacode.chatt.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageHolder extends RecyclerView.ViewHolder {

    private TextView time;
    private TextView message;
    private CircleImageView general_user_pic;
    private ImageView photo_message;

    public MessageHolder(@NonNull View itemView) {
        super(itemView);

        time = itemView.findViewById(R.id.tv_time);
        message= itemView.findViewById(R.id.tv_message);
        general_user_pic = itemView.findViewById(R.id.civ_general_user_pic);
        photo_message = itemView.findViewById(R.id.iv_photo_message);

    }

    public TextView getTime() {
        return time;
    }

    public void setTime(TextView time) {
        this.time = time;
    }

    public TextView getMessage() {
        return message;
    }

    public void setMessage(TextView message) {
        this.message = message;
    }

    public CircleImageView getGeneral_user_pic() {
        return general_user_pic;
    }

    public void setGeneral_user_pic(CircleImageView general_user_pic) {
        this.general_user_pic = general_user_pic;
    }

    public ImageView getPhoto_message() {
        return photo_message;
    }

    public void setPhoto_message(ImageView photo_message) {
        this.photo_message = photo_message;
    }
}
