package com.andreacode.chatt.data.model;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {

    private String content;
    private String url_photo;
    private String sender_id;
    private String receiver_id;
    private Date creation_date;

    public Message() {
    }

    public Message(String content, String url_photo, String sender_id, String receiver_id) {
        this.content = content;
        this.url_photo = url_photo;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    //This method call is from MessageAdapter
    public String getTime(){
        Date hour = getCreation_date();
        SimpleDateFormat sdf = new SimpleDateFormat("kk:mm");

        return sdf.format(hour);
    }
}
