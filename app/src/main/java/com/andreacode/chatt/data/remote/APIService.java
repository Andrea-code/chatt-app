package com.andreacode.chatt.data.remote;

import com.andreacode.chatt.data.model.Message;
import com.andreacode.chatt.data.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIService {

    // U S E R

    @POST("/user")
    Call<Void> newUser(@Body User user);

    @GET("/users")
    Call<List<User>> getAllUsers();

    @GET("/user/{id}")
    Call<User> getUserById(@Path("id") String id);

    // C H A T

    @POST("/message")
    Call<Void>newMessagePrivateChat(@Body Message message);

    @GET("/messages/{iudSender}/{uidReceiver}")
    Call<List<Message>> getMessages(@Path("iudSender") String iudSender, @Path("uidReceiver") String uidReceiver);

    // C O N V E R S A T I O N S

    @GET("/messages/{id}")
    Call<List<User>> getConvertationsById(@Path( "id") String id);

}
