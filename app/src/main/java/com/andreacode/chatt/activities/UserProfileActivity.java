package com.andreacode.chatt.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andreacode.chatt.R;
import com.andreacode.chatt.data.model.User;
import com.andreacode.chatt.data.remote.APIService;
import com.andreacode.chatt.data.remote.APIUtils;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity {
    private APIService apiService;
    private ImageButton iv_chat_user;
    private User user;
    private Bundle bundle;

    private CircleImageView profile_image;
    private TextView tv_user_name;
    private TextView tv_description_user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        apiService = APIUtils.getAPIService();

        iv_chat_user = findViewById(R.id.iv_chat_user);
        profile_image = findViewById(R.id.profile_image);
        tv_user_name = findViewById(R.id.tv_user_name);
        tv_description_user = findViewById(R.id.tv_description_user);

        //tv_description_user.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD); API >= 26!!

        bundle = getIntent().getExtras();
        final String uid_selected_user = bundle.getString("uid_selected_user");

        getDataCurrentUser(uid_selected_user);

        iv_chat_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserProfileActivity.this, ChatActivity.class);
                intent.putExtra("uid_selected_user", uid_selected_user);
                intent.putExtra("name_selected_user", user.getName());
                startActivity(intent);
            }
        });
    }


    private void getDataCurrentUser(String id) {
        apiService.getUserById(id).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.i("HOME ACTIVITY", "get user fromAPI: " + response.body().getEmail());
                user = response.body();

                if (user.getProfilePhoto() != null) {
                    Picasso.get()
                            .load(user.getProfilePhoto())
                            .into(profile_image);
                }

                tv_user_name.setText(user.getName());
                tv_description_user.setText(user.getDescription());

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("HOME ACTIVITY", "Fail to make the GET");
            }
        });
    }


}
