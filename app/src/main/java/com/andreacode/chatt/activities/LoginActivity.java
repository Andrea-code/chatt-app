package com.andreacode.chatt.activities;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andreacode.chatt.Current;
import com.andreacode.chatt.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth fire_auth;
    private EditText et_email, et_password;
    private Button go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_email = findViewById(R.id.et_field_email);
        et_password = findViewById(R.id.et_field_password);


        go = findViewById(R.id.btn_send);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = et_email.getText().toString().trim();
                String password = et_password.getText().toString();
                login(email, password);
            }
        });

        fire_auth = FirebaseAuth.getInstance();
    }


    private void login(String email, String password) {

        if (validateLogin(email, password)) {
            fire_auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("firebase login", "signInWithEmail:success");

                                FirebaseUser user = fire_auth.getCurrentUser();
                                Current.user = user;

                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("firebase login", "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        } else {
            Log.i("validation login", "hubo algun error en la validacion");
        }
    }


    private boolean validateLogin(String email, String password) {

        boolean valid = true;

        if (TextUtils.isEmpty(email)) {
            et_email.setError(getString(R.string.et_empty_error));
            valid = false;
        } else if (!isEmailValid(email)) {
            et_email.setError(getString(R.string.et_email_error));
            valid = false;
        } else {
            et_email.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            et_password.setError(getString(R.string.et_empty_error));
            valid = false;
        } else {
            et_password.setError(null);
        }

        return valid;
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
