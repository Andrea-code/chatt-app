package com.andreacode.chatt.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.andreacode.chatt.R;
import com.andreacode.chatt.adapter.UsersAdapter;
import com.andreacode.chatt.data.model.User;
import com.andreacode.chatt.data.remote.APIService;
import com.andreacode.chatt.data.remote.APIUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersActivity extends AppCompatActivity {
    private APIService apiService;

    private UsersAdapter usersAdapter;
    private RecyclerView rv_users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        rv_users = findViewById(R.id.rv_users);

        apiService = APIUtils.getAPIService();

        getAllUsers();
    }


    public void getAllUsers() {
        apiService.getAllUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                //Log.i("UsersActivity", "" + response.code());
                showUsers(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("UsersActivity ACTIVITY", "Fail to make the GET ALL USERS");
            }
        });
    }

    public void showUsers(List<User> users) {
        usersAdapter = new UsersAdapter(UsersActivity.this, users);
        usersAdapter.notifyDataSetChanged();
        rv_users.setLayoutManager(new GridLayoutManager(UsersActivity.this, 3));
        rv_users.setAdapter(usersAdapter);

        usersAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = rv_users.getChildAdapterPosition(view);
                String uid_selected_user = usersAdapter.getUsers().get(position).getUid();

                Intent intent = new Intent(UsersActivity.this, UserProfileActivity.class);
                intent.putExtra("uid_selected_user", uid_selected_user);
                startActivity(intent);
            }
        });
    }


}
