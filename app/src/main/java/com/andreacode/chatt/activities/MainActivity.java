package com.andreacode.chatt.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreacode.chatt.R;


public class MainActivity extends AppCompatActivity {

    private Animation bottomToUp;
    private ImageView image;
    private TextView description1, description2;
    private Button signIn, login;
    private Thread threadFirstBubble, threadSecondBubble, threadBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        image = findViewById(R.id.iv_girl_phone);
        description1 = findViewById(R.id.app_description1);
        description2 = findViewById(R.id.app_description2);

        signIn = findViewById(R.id.btn_signin);
        login = findViewById(R.id.btn_login);

        bottomToUp = AnimationUtils.loadAnimation(this, R.anim.down_to_up);
        bottomToUp.setDuration(2000);

        image.setAnimation(bottomToUp);

        threadFirstBubble = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        description1.setVisibility(View.VISIBLE);
                    }
                });
            }
        };

        threadSecondBubble = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        description2.setVisibility(View.VISIBLE);
                    }
                });
            }
        };

        threadBtn = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3500);
                } catch (InterruptedException e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        signIn.setVisibility(View.VISIBLE);
                        login.setVisibility(View.VISIBLE);
                    }
                });
            }
        };

        threadFirstBubble.start();
        threadSecondBubble.start();
        threadBtn.start();

    }


    public void signIn(View view) {
        Intent intent = new Intent(this, FormSignInActivity.class);
        startActivity(intent);
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
