package com.andreacode.chatt.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andreacode.chatt.Current;
import com.andreacode.chatt.R;
import com.andreacode.chatt.data.model.User;
import com.andreacode.chatt.data.remote.APIService;
import com.andreacode.chatt.data.remote.APIUtils;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    private APIService apiService;
    private User user;
    private Bundle bundle;
    private TextView tv_user_name, tv_description_user;
    private CircleImageView my_profile_image;
    private ImageButton iv_conversations, iv_users;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //INSTANCES
        my_profile_image = findViewById(R.id.my_profile_image);
        tv_user_name = findViewById(R.id.tv_user_name);
        tv_description_user = findViewById(R.id.tv_description_user);
        iv_conversations = findViewById(R.id.iv_conversations);
        iv_users = findViewById(R.id.iv_users);

        apiService = APIUtils.getAPIService();

        //For prevention
        if (Current.user != null) {
            String id_current_user = Current.user.getUid();
            //GET
            getDataCurrentUser(id_current_user);
        } else {

            finish();
        }


    }

    private void getDataCurrentUser(String id) {
        apiService.getUserById(id).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                user = response.body();
                if (user.getProfilePhoto() != null) {
                    Picasso.get()
                            .load(user.getProfilePhoto())
                            .into(my_profile_image);
                }
                tv_user_name.setText(user.getName());
                tv_description_user.setText(user.getDescription());

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("HOME ACTIVITY", "Fail to make the GET");
            }
        });
    }


    public void actionButton(View v) {
        switch (v.getId()) {
            case R.id.iv_users:
                intent = new Intent(HomeActivity.this, UsersActivity.class);
                startActivity(intent);

                break;
            case R.id.iv_conversations:
                intent = new Intent(HomeActivity.this, ConversationsListActivity.class);
                startActivity(intent);

                break;
        }
    }

}
