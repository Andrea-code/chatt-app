package com.andreacode.chatt.activities;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.andreacode.chatt.Current;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andreacode.chatt.R;
import com.andreacode.chatt.data.model.User;
import com.andreacode.chatt.data.remote.APIService;
import com.andreacode.chatt.data.remote.APIUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormSignInActivity extends AppCompatActivity {

    private APIService apiService;
    private Task<Uri> remoteUri;
    private FirebaseAuth fire_auth;
    private StorageReference storageRef;
    private EditText et_name, et_email, et_password, et_confirm_pass, et_field_about;
    private String name, email, password, confirmPass, description, url_image;
    private Uri uri_image_profile;
    private CircleImageView civ_profile_image;
    private Button btn_signin;
    private final int PICK_IMAGE_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.do_not_move, R.anim.do_not_move);
        setContentView(R.layout.activity_form_sign_in);

        //INSTANCES
        civ_profile_image = findViewById(R.id.civ_profile_image);
        et_name = findViewById(R.id.et_field_name);
        et_email = findViewById(R.id.et_field_email);
        et_password = findViewById(R.id.et_field_password);
        et_confirm_pass = findViewById(R.id.et_field_repeat_password);
        et_field_about = findViewById(R.id.et_field_about);
        btn_signin = findViewById(R.id.btn_send);

        apiService = APIUtils.getAPIService();
        fire_auth = FirebaseAuth.getInstance();


        //LOAD AND IMAGE FROM GALLERY
        civ_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureChooser();
            }
        });

        // BUTTON SING IN ACTION
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });

    }

    /**
     * Validates the required field for an user registration
     * @param name
     * @param email
     * @param password
     * @param confirmPass
     * @return
     */
    private boolean formValidation(String name,String email, String password, String confirmPass) {
        boolean valid = true;

        if (TextUtils.isEmpty(name)) {
            et_name.setError(getString(R.string.et_empty_error));
            valid = false;
        } else {
            et_name.setError(null);
        }

        if (TextUtils.isEmpty(email)) {
            et_email.setError(getString(R.string.et_empty_error));
            valid = false;
        } else if (!isEmailValid(email)) {
            et_email.setError(getString(R.string.et_email_error));
            valid = false;
        }

        if (TextUtils.isEmpty(password)) {
            et_password.setError(getString(R.string.et_empty_error));
            valid = false;
        } else if (password.length() < 8) {
            et_password.setError(getString(R.string.et_password_size_error));
            valid = false;
        } else {
            et_password.setError(null);
        }

        if (TextUtils.isEmpty(confirmPass)) {
            et_confirm_pass.setError(getString(R.string.et_empty_error));
            valid = false;
        } else {
            et_confirm_pass.setError(null);
        }

        //Match between password and confirmation password
        if (!password.equals(confirmPass)) {
            et_confirm_pass.setError(getString(R.string.et_password_error));
            valid = false;
        }else{
            et_confirm_pass.setError(null);
        }

        return valid;
    }


    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void pictureChooser(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;
        StorageReference stRef, photoRef;

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {

            uri_image_profile = data.getData();
            civ_profile_image.setImageURI(uri_image_profile);

            uri = data.getData();
            stRef = FirebaseStorage.getInstance().getReference("profile_photo_users");
            photoRef = stRef.child(uri.getLastPathSegment());
            photoRef.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    remoteUri = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                    remoteUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            url_image = uri.toString();
                        }
                    });
                }
            });
        }
    }

    /**
     * Call the method newUser which mades the POST petition
     * from API Service interface
     * @param user
     */
    private void sendPOST(User user){
        apiService.newUser(user).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    Log.i("FORM SIGNIN ACTIVITY", "User submitted to API." + response.body());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("FORM SIGNIN ACTIVITY", "Fail to make the POST");
            }
        });
    }


    /**
     * Verifies the user data from sign up form
     * and add a new user to firebase
     */
    public void createUser() {
        name = et_name.getText().toString().trim();
        email = et_email.getText().toString().trim();
        password = et_password.getText().toString().trim();
        confirmPass = et_confirm_pass.getText().toString().trim();
        description = et_field_about.getText().toString();

        if (formValidation(name,email, password, confirmPass)) {
            Log.i("formvalidation: ", ""+formValidation(name,email, password,confirmPass));
            fire_auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                Log.d("firebase", "createUserWithEmail: success");

                                // Sign in success, update UI with the signed-in user's information
                                FirebaseUser user = fire_auth.getCurrentUser();
                                Current.user = user;

                                //Prepare the Object to send it to Firebase
                                User user_POST = new User();
                                user_POST.setUid(user.getUid());
                                user_POST.setName(name);
                                user_POST.setEmail(email);
                                user_POST.setDescription(description);
                                user_POST.setProfilePhoto(url_image);


                                sendPOST(user_POST);

                                Bundle bundle = new Bundle();
                                bundle.putString("uid_current_user", user.getUid());

                                Intent intent = new Intent(FormSignInActivity.this, HomeActivity.class);
                                intent.putExtras(bundle);

                                //I give time for POST finished
                                try {
                                    Thread.sleep(900);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                startActivity(intent);
                                finish();

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("firebase", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(FormSignInActivity.this, R.string.toast_login_error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }
}