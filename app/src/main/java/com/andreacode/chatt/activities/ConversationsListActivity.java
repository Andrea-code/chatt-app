package com.andreacode.chatt.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.andreacode.chatt.Current;
import com.andreacode.chatt.R;
import com.andreacode.chatt.adapter.ConversationAdapter;
import com.andreacode.chatt.data.model.User;
import com.andreacode.chatt.data.remote.APIService;
import com.andreacode.chatt.data.remote.APIUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConversationsListActivity extends AppCompatActivity {

    private APIService apiService;
    private RecyclerView rv_conversations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversations_list);

        rv_conversations = findViewById(R.id.rv_conversations);

        apiService = APIUtils.getAPIService();

        if(Current.user != null){
            String id_current_user = Current.user.getUid();
            getConversations( id_current_user );
        }
    }


    public void getConversations(String uid){
        apiService.getConvertationsById(uid).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                displayListConversations(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("CONVERSATIONS ACTIVITY", "GET FAILS");
            }
        });
    }


    public void displayListConversations(List<User> users){
        final ConversationAdapter adapter = new ConversationAdapter(ConversationsListActivity.this, users);
        adapter.notifyDataSetChanged();
        rv_conversations.setLayoutManager(new LinearLayoutManager(this));
        rv_conversations.setAdapter(adapter);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = rv_conversations.getChildAdapterPosition(view);
                String uid_selected_user = adapter.getUsers().get(position).getUid();
                String name_selected_user = adapter.getUsers().get(position).getName();

                Intent intent = new Intent(ConversationsListActivity.this, ChatActivity.class);
                intent.putExtra("uid_selected_user", uid_selected_user);
                intent.putExtra("name_selected_user", name_selected_user);
                startActivity(intent);
            }
        });

    }

}
