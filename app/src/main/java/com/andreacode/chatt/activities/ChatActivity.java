package com.andreacode.chatt.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.andreacode.chatt.Current;
import com.andreacode.chatt.R;
import com.andreacode.chatt.adapter.MessageAdapter;
import com.andreacode.chatt.data.model.Message;
import com.andreacode.chatt.data.model.User;
import com.andreacode.chatt.data.remote.APIService;
import com.andreacode.chatt.data.remote.APIUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {
    private APIService apiService;
    private MessageAdapter messageAdapter;
    private FirebaseFirestore db;
    private DatabaseReference dbRef;
    private Message message;
    private Bundle bundle;
    private User user;

    private TextView user_name;
    private RecyclerView rv_messages;
    private Button btn_send;
    private ImageButton ibtn_send_image;
    private EditText et_input_message;

    private String content;
    private String uid_selected_user;
    private Task<Uri> remoteUri;
    private final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //"Nav"
        user_name = findViewById(R.id.tv_sec_username);

        //Bottom
        rv_messages = findViewById(R.id.rv_messages);
        ibtn_send_image = findViewById(R.id.ibtn_send_image);
        et_input_message = findViewById(R.id.et_input_message);
        btn_send = findViewById(R.id.btn_send);

        apiService = APIUtils.getAPIService();
        messageAdapter = new MessageAdapter(this);

        dbRef = FirebaseDatabase.getInstance().getReference("chat_photos");

        //Information for contruct our message object
        bundle = getIntent().getExtras();
        uid_selected_user = bundle.getString("uid_selected_user");
        user_name.setText(bundle.getString("name_selected_user"));


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_messages.setLayoutManager(linearLayoutManager);
        rv_messages.setAdapter(messageAdapter);


        //This method is listening for changes.
        messageAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScrollBar();
            }
        });


        db = FirebaseFirestore.getInstance();

        //Load pictures
        ibtn_send_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        });

        // Prepare listener for real time changes
        CollectionReference colRef = db.collection("chat")
                .document(Current.user.getUid())
                .collection(uid_selected_user);

        colRef.orderBy("date", Query.Direction.ASCENDING).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                List<Message> messageList = new ArrayList<>();

                if (!queryDocumentSnapshots.isEmpty()) {
                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        Message message = new Message();

                        if (document.getData().get("content") != null) {
                            message.setContent(document.getData().get("content").toString());
                        }

                        if (document.getData().get("url_photo") != null) {
                            message.setUrl_photo(document.getData().get("url_photo").toString());
                        }

                        if (document.getTimestamp("date") != null) {
                            message.setCreation_date(document.getTimestamp("date").toDate());
                        }
                        message.setSender_id(document.getData().get("sender_id").toString());
                        message.setReceiver_id(document.getData().get("receiver_id").toString());

                        messageList.add(message);
                    }
                    messageAdapter.addMessage(messageList);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri;
        StorageReference stRef, photoRef;

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            uri = data.getData();
            stRef = FirebaseStorage.getInstance().getReference("chat_photos");
            photoRef = stRef.child(uri.getLastPathSegment());
            photoRef.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    remoteUri = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                    remoteUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Message messagePhoto = new Message(null, uri.toString(), Current.user.getUid(), uid_selected_user);
                            createMessage(messagePhoto);
                        }
                    });
                }
            });
        }
    }

    private void setScrollBar() {
        rv_messages.scrollToPosition(messageAdapter.getItemCount() - 1);
    }

    public void sendMessage(View view) {
        content = et_input_message.getText().toString();

        if (!content.isEmpty()) {
            message = new Message(content, null, Current.user.getUid(), uid_selected_user);
            createMessage(message);
        }

        if (et_input_message.length() > 0) {
            et_input_message.getText().clear();
        }
    }


    private void createMessage(Message message) {
        apiService.newMessagePrivateChat(message).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(ChatActivity.this, "onResponse", Toast.LENGTH_SHORT).show();

                if (response.isSuccessful()) {
                    //Toast.makeText(ChatActivity.this, "SUCCESS POST", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(ChatActivity.this, "ERROR POST" + response.code(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                //Toast.makeText(ChatActivity.this, "FAIL POST", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
