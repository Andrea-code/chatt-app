# Real time Chatt, Native app for Android
**This is a simple chat that allows user registration, login, send messages and photos to other users.**


Tecnologies was used:
* Java 8
* Android
* Firebase Cloud Firestore
* Firebase Authentication
* Firebase Cloud Storage
* Retrofit
* Picasso Library

This app connects to an [API Rest I made here](https://gitlab.com/Andrea-code/api_chatt),
I recommend to see it to understand how is the structure of database.
I use Firebase for know how it works with Android development app and how it works inside the console.
And last, I used Picasso for all relationated with download the images from Firebase.

This app was made for a personal educational purpose
and, I hope helps someone who is learning too :)

**IMPORTANT**
For security reasons, the file .json from Firebase and the URL (where deploy the API) have been deleted.
Please add the file and url with your own Firebase project.


Activities screenshots:


![home](https://user-images.githubusercontent.com/37070594/62303843-29ff0000-b47d-11e9-9c56-db8dee42b0c9.png)
![login](https://user-images.githubusercontent.com/37070594/62303850-2c615a00-b47d-11e9-9191-377b7a8eb737.png)
![signin](https://user-images.githubusercontent.com/37070594/62303855-2ec3b400-b47d-11e9-903f-ed9c8fefc49c.png)
![user_profile](https://user-images.githubusercontent.com/37070594/62303856-2ec3b400-b47d-11e9-85a3-fca977af3c75.png)
![users](https://user-images.githubusercontent.com/37070594/62303857-2f5c4a80-b47d-11e9-8250-4062d372fca7.png)
![chat](https://user-images.githubusercontent.com/37070594/62303858-2f5c4a80-b47d-11e9-8ee9-86d330aa8b43.png)
![conversations](https://user-images.githubusercontent.com/37070594/62303859-2f5c4a80-b47d-11e9-8092-d320a609e1f1.png)

Resources:
* Icons: [FlatIcon](https://www.flaticon.com/)
* Illustrations: [Marginalia](https://icons8.com/ouch/style/marginalia)
